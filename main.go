package main

import (
    "gitlab.com/matthewhughes/trial_go_project/internal"
)

func main() {
    internal.Hello()
    internal.Goodbye()
}

