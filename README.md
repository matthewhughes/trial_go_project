# Trial Package

A trial package so I can make sense of `go`, some development commands:

``` shell
$ go run gitlab.com/matthewhughes/trial_go_project
Hello
Goodbye
$ go test ./...
?   	gitlab.com/matthewhughes/trial_go_project	[no test files]
ok  	gitlab.com/matthewhughes/trial_go_project/internal	0.001s
```

The `...` is a wildcard for matching any package, from `go help packages`:

> An import path is a pattern if it includes one or more "..." wildcards, each
> of which can match any string, including the empty string and strings
> containing slashes. Such a pattern expands to all package directories found in
> the GOPATH trees with names matching the patterns.

Installing:

``` shell
$ go install gitlab.com/matthewhughes/trial_go_project
$ trial_go_project
Hello
Goodbye
```

## Testing and Debugging with GDB

Compile the package without optimisations or inlining (see `go doc cmd/compile`
for flag meanings):

``` shell
$ go test -gcflags='-N -l' -c gitlab.com/matthewhughes/trial_go_project/internal
$ gdb ./internal.test
```

```gdb
# break on a line
(gdb) break internal/goodbye.go:12
# or a functions
(gdb) break mjh/trial_pkg/internal.foo
# get details about functions
(gdb) info functions gitlab.com/*
```
