package internal

import (
    "testing"
)

func TestFoo(t *testing.T) {
    expected := 12
    if got := foo(); got != expected {
        t.Errorf("foo() got %d, expected %d", got, expected)
    }
}
