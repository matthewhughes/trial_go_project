package internal

import (
    "fmt"
)

func Goodbye() {
    fmt.Println("Goodbye")
}

// This function is just for testing
func foo() int {
    return 12
}


